package com.veritran.kata.marsrover.core

class Rover(private var position: Position, private val facingTowards: String) {
    fun getPosition(): Position {
        return position
    }

    fun moveForward() {
        if (facingTowards == "N") {
            position = position.moveNorth()
            return
        }
        position = position.moveSouth()
    }

}
