package com.veritran.kata.marsrover.core

data class Position(val axisX: Int, val axisY: Int) {
    fun moveNorth(): Position {
        return Position(axisX, axisY + 1)
    }

    fun moveSouth(): Position {
        return Position(axisX, axisY - 1)
    }

}
