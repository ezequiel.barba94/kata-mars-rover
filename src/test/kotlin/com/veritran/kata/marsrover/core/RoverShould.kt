package com.veritran.kata.marsrover.core

import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test

class RoverShould {
    @Test
    fun `move forward facing north`() {
        val rover = Rover(Position(0, 0), "N")

        rover.moveForward()

        val position = rover.getPosition()

        position `should be equal to` Position(0, 1)
    }

    @Test
    fun `move forward facing south`() {
        val rover = Rover(Position(0, 0), "S")

        rover.moveForward()

        val position = rover.getPosition()

        position `should be equal to` Position(0, -1)
    }
}